var loginForm = document.getElementById("loginForm")
var pleaseWaitDiv = document.getElementById("pleaseWaitDiv")
var instructionsDiv = document.getElementById("instructionsDiv")
var instructionsTextarea = document.getElementById("instructionsTextarea")
var inProgressDiv = document.getElementById("inProgressDiv")
var completeDiv = document.getElementById("completeDiv")
var socket = io();

var id
var timeStep = 0.1
var treatment = 1
var state = "startup"
var action = 0
var payoff = 0
var maximize = false

// Disable Right Click Menu
document.oncontextmenu = () => false

document.onmousedown = function(event) {
  console.log(`action = ${action}`)
  console.log(`state = ${state}`)
  if(maximize) goFullScreen();
}

document.onmousemove = function(event) {
  action = (10*(event.x/window.innerWidth)).toFixed(0)
}

socket.on('updateSubject', function (msg) {
  treatment = msg.treatment
  state = msg.state
  payoff = msg.payoff
  updateinterface() 
});

updateinterface = function() {
  if(instructionsTextarea.innerHTML!=instructions) {
    instructionsTextarea.innerHTML = instructions  
  }
  loginForm.style.display = "none"
  pleaseWaitDiv.style.display = "none"
  instructionsDiv.style.display = "none"
  inProgressDiv.style.display = "none"
  completeDiv.style.display = "none"
  if(state=="startup") {
    pleaseWaitDiv.style.display = "block"
  }
  if(state=="instructions") {
    instructionsDiv.style.display = "block"
  }
  if(state=="experimentInProgress") {
    inProgressDiv.style.display = "block"
    inProgressDiv.innerHTML = `<br> <br> Your Action: ${action} <br> <br> Your Payoff: ${payoff}`
  }
  if(state=="experimentComplete") {
    completeDiv.style.display = "block"
    completeDiv.innerHTML = `<br> <br> The experiment is complete. <br> <br> Please wait while your payment is prepared.`
  }
}

function goFullScreen(){
    if(document.documentElement.requestFullScreen)
        document.documentElement.requestFullScreen()
    else if(document.documentElement.webkitRequestFullScreen)
        document.documentElement.webkitRequestFullScreen()
    else if(document.documentElement.mozRequestFullScreen)
        document.documentElement.mozRequestFullScreen()
}

function joinGame() {
  console.log("joinGame")
  id = parseInt(loginForm["subjectID"].value)
  if(id>0) {
    setInterval(updateServer,timeStep*1000)
    loginForm.style.display = "none"
    if(maximize) setTimeout(goFullScreen,200)
  }
  return false;
}

function updateServer() {
  var msg = { 
    id: id, 
    action: action
  }
  socket.emit('clientUpdateServer',msg)
}