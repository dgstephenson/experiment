var loginForm = document.getElementById("loginForm")
var subjectsTable = document.getElementById("subjectsTable")
var controlsTable = document.getElementById("controlsTable")
var treatmentRadio1 = document.getElementById("treatmentRadio1")
var treatmentRadio2 = document.getElementById("treatmentRadio2")
var stateLabel = document.getElementById("stateLabel")
var socket = io()

var timeStep = 0.1
var instructions = ""
var ids = []
var numSubjects = 0
var treatment = 1
var state = "startup"

socket.on('updateManager', function (msg) {
  ids = msg.ids
  numSubjects = msg.numSubjects
  state = msg.state
  stateLabel.innerHTML = state;
  var tableString = ""
  ids.forEach(id=>{if(id>0) tableString+=`<tr><td>${id}</td></tr>`})
  subjectsTable.innerHTML = tableString;
  loginForm.style.display = "none"
  subjectsTable.style.display = "table"
  controlsTable.style.display = "table"
});

function showInstructions() {
  if(state=='startup') {
    socket.emit('showInstructions')
  }
}

function beginExperiment() {
  if(state=='instructions') {
    socket.emit('beginExperiment')
  }
}

function endExperiment() {
  if(state=='experimentInProgress') {
    socket.emit('endExperiment')
  }
}

function connectToServer() {
  console.log("connectToServer")
  setInterval( managerUpdateServer, timeStep*1000);
  return false;
}

function managerUpdateServer() {
  if(treatmentRadio1.checked) treatment = 1
  if(treatmentRadio2.checked) treatment = 2
  var msg = {
    treatment: treatment
  };
  socket.emit('managerUpdateServer',msg)
}