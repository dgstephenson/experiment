var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');

var subjects = [];
var numSubjects = 0;
var dateString; 
var dataStream;
var state = "startup"
var treatment = 1

app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
   res.sendFile(__dirname + '/public/client.html');
})

app.get('/manager', function (req, res) {
   res.sendFile(__dirname + '/public/manager.html');
})

io.on('connection', function(socket){
  socket.on('clientUpdateServer', function(msg){
    if(typeof(subjects[msg.id]) === "undefined") {
      createSubject(msg.id);
    } else {
      subjects[msg.id].action = msg.action
      subjects[msg.id].payoff = 10-Math.abs(msg.action-5)
      updateSubject(msg.id,socket)
    }
  });
  socket.on('managerUpdateServer', function(msg){
    treatment = msg.treatment;
    updateManager(socket);
  });
  socket.on('showInstructions', function() {
    state = "instructions";
  });
  socket.on('beginExperiment', function(msg){
    state = "experimentInProgress";
  });
  socket.on('endExperiment', function(msg){
    state = "experimentComplete";
    updateDataFile()
    writePaymentFile()
  });
});

createSubject = function(id) {
  numSubjects += 1
  subjects[id] = {
    id: id,
    action: 0,
    payoff: 0
  };
  console.log(id+' connected');
}

updateSubject = function(id,socket) {
  var subject = subjects[id];
  var msg = {
    treatment: treatment,
    state: state,
    payoff: subject.payoff
  }
  socket.emit("updateSubject",msg);
}

getSubjectIds = function() {
  var ids = subjects.map(subject => subject.id).sort((a,b)=>a-b);
  ids.pop();
  return ids;
}

updateManager = function(socket) {
  const ids = getSubjectIds()
  const msg = {
    ids: ids,
    numSubjects: numSubjects,
    state: state
  };
  socket.emit("updateManager",msg);
}

getDateString = function() {
  const makeTwoDigits = function(x) {
    y = Math.round(x);
    if(y>9) return String(y);
    else return String("0"+y);
  }
  var d = new Date();
  var myDateString = "";
  myDateString += d.getFullYear();
  myDateString += makeTwoDigits((d.getMonth()+1));
  myDateString += makeTwoDigits(d.getDate());
  myDateString += makeTwoDigits(d.getHours());
  myDateString += makeTwoDigits(d.getMinutes());
  myDateString += makeTwoDigits(d.getSeconds());
  return myDateString;
}

createDataFile = function() {
  dataStream = fs.createWriteStream("data/"+dateString+"-data.csv");
  var csvString = "session,treatment,subject,action,payoff\n";
  const logErr = (err) => { if(err){console.log(err)} };
  dataStream.write(csvString, logErr);
}

updateDataFile = function() {
  subjects.forEach(subject => {
    var csvString = ""+dateString;
    csvString += ","+treatment;
    csvString += ","+subject.id;
    csvString += ","+subject.action;
    csvString += ","+subject.payoff;
    csvString += "\n";
    const logErr = (err) => { if(err){console.log(err)} };
    dataStream.write(csvString, logErr);
  })
}

writePaymentFile = function() {
  var csvString = "subject,payment\n";
  subjects.forEach(subject => {
    csvString += ""+subject.id+","+subject.payoff.toFixed(2)+"\n";
  })
  const logErr = (err) => { if(err){console.log(err)} };
  fs.writeFile("data/"+dateString+"-payment.csv",csvString, logErr);
}

http.listen(3000, function(){
  var port = http.address().port
  console.log('listening on *:%s', port);
  dateString = getDateString();
  createDataFile()
})